const callback3 = require('../callback3.js');
const cards = '../cards.json';
const listId = 'qwsa221';

callback3.getCardsDetails(listId, cards)
    .then(function (cardDetails) {
        const actualResult = cardDetails;
        const expectedResult = [
            {
                id: '1',
                description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
            },
            {
                id: '2',
                description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
            },
            {
                id: '3',
                description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
            }
        ];

        if (JSON.stringify(actualResult) === JSON.stringify(expectedResult)) {
            console.log(actualResult);
        } else {
            console.log('Actual Result is not as expected');
        }
    })
