const boardFn = require('../callback1.js');
const boards = '../boards.json';
const id='mcu453ed';

boardFn.getBoardInformationById(id, boards)
    .then(function (data) {
        const actualResult = data;
        const expectedResult = { id: 'mcu453ed', name: 'Thanos', permissions: {} };
        if (JSON.stringify(actualResult) === JSON.stringify(expectedResult)) {
            console.log(actualResult);
        } else {
            console.log('The result is not what expected');
        }
    }).catch(function (err) {
        console.log(error);
    });
