const lists = '../lists.json';
const boards = 'mcu453ed';
const callback2 = require('../callback2.js')

callback2.getListFromBoard(lists, boards)
    .then(function (newData) {
        const actualResult = newData;
        const expectedResult = [
            { id: 'qwsa221', name: 'Mind' },
            { id: 'jwkh245', name: 'Space' },
            { id: 'azxs123', name: 'Soul' },
            { id: 'cffv432', name: 'Time' },
            { id: 'ghnb768', name: 'Power' },
            { id: 'isks839', name: 'Reality' }
          ]
        if(JSON.stringify(actualResult)===JSON.stringify(expectedResult)){
            console.log(actualResult);
        }else{
            console.log('Results are not as expected');
        }

    })
