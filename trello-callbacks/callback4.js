const callback3 = require('./callback3.js');
const callback2 = require('./callback2.js');
const callback1 = require('./callback1.js');
const boards = '../boards.json';
const lists = '../lists.json';
const cards = '../cards.json';

async function getInformationOfThanos(thanosId, mind) {

    try {
        const details = await callback1.getBoardInformationById(thanosId, boards)
        console.log(details);

        const data = await callback2.getListFromBoard(lists, details.id);

        console.log(data);
        const mindData = data.find(element => element.name === mind);
        const cardDetails = await callback3.getCardsDetails(mindData.id, cards);
        console.log(cardDetails);

    } catch (e) {
        return e;
    }

}

module.exports = { getInformationOfThanos };
