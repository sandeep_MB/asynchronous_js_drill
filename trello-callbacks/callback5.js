const callback1 = require('./callback1.js');
const callback2 = require('./callback2.js');
const callback3 = require('./callback3.js');

const boards = '../boards.json';
const lists = '../lists.json';
const cards = '../cards.json';


async function getCardDetailsOfMindSpace(thanosId, mind, space) {

    try {
        const thanoDetails = await callback1.getBoardInformationById(thanosId, boards);
        console.log(thanoDetails);

        const listDetails = await callback2.getListFromBoard(lists, thanoDetails.id);
        console.log(listDetails);

        const mindDetails = listDetails.find(element => element.name === mind);
        const cardsByMind = await callback3.getCardsDetails(mindDetails.id, cards);
        console.log(cardsByMind);

        const spaceDetails = listDetails.find(element => element.name === space);
        const cardsBySpace = await callback3.getCardsDetails(spaceDetails.id, cards);
        console.log(cardsBySpace);
        return true;

    } catch (err) {
        return err;
    }

}

module.exports = { getCardDetailsOfMindSpace }