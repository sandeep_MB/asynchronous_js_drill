const callback1 = require('./callback1.js');
const callback2 = require('./callback2.js');
const callback3 = require('./callback3.js');
const boards = '../boards.json';
const lists = '../lists.json';
const cards = '../cards.json';

async function getCardDetailsOfAllIds(thanosId) {

    try {
        const thanoDetails = await callback1.getBoardInformationById(thanosId, boards);
        console.log(thanoDetails);

        const listDetails = await callback2.getListFromBoard(lists, thanoDetails.id);
        console.log(listDetails);

        const promises = listDetails.map(function (element) {
            return callback3.getCardsDetails(element.id, cards);
        });
        Promise.all(promises).then(function (data) { console.log(data) });

    }
    catch (err) {
        return err;
    }
}

module.exports = { getCardDetailsOfAllIds }