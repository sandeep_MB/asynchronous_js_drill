const fs = require('fs');

function getCardsDetails(listId, cards) {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            fs.readFile(cards, 'utf-8', function (err, cardData) {
                let cardDataObject = JSON.parse(cardData);
                if (err) {
                    reject(new Error(err));
                } else {
                    resolve(cardDataObject[listId]);
                }
            });

        }, 2000);
    })
}

module.exports = { getCardsDetails };