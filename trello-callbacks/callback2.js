const fs = require('fs');

function getListFromBoard(lists, boardId) {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            fs.readFile(lists, 'utf-8', function (err, data) {
                let listData = JSON.parse(data);
                if (err) {
                    reject(new Error(err));
                } else {
                    resolve(listData[boardId]);
                }
            });
        }, 2000);
    })
}

module.exports = { getListFromBoard };