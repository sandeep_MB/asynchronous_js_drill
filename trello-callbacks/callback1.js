const fs = require('fs');

function getBoardInformationById(id, boards) {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            fs.readFile(boards, 'utf-8', function (err, data) {
                if (err) {
                    reject(new Error(err));
                } else {
                    let file = JSON.parse(data);
                    file.map(function (board) {
                        if (board.id === id) {
                            resolve(board);
                        }
                    });
                }
            });
        }, 2000);

    })
}

module.exports = { getBoardInformationById }