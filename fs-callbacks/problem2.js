const fs = require('fs');

const inputFile = './lipsum.txt';

readGivenFile(inputFile)
    .then(data => convertToUpperCase(data))
    .then(data2 => lowerCaseAndSplitInSentences(data2))
    .then((data3) => {
        sorttheContentInFile(data3);
        deletAllFilesInFilenames();
    })
    .catch(err => { console.error(err.message) });

// 1. Read the given file lipsum.txt
function readGivenFile(inputFile) {
    return new Promise((resolve, reject) => {
        fs.readFile(inputFile, 'utf-8', function (err, data) {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        })
    })
}


// 2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
function convertToUpperCase(file) {
    return new Promise(function (resolve, reject) {

        const upperCase = file.toUpperCase();
        // console.log(upperCase);
        writeContentInFile('./outputFiles/upperCase.txt', upperCase);
        writeFileNames('./filenames.txt', './outputFiles/upperCase.txt\n');
        resolve(upperCase);
    })
}


// 3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
function lowerCaseAndSplitInSentences(file) {
    return new Promise(function (resolve, reject) {
        const lowerCaseData = file.toLowerCase().replace(/[\r\n]+/gm, " ").split(".").join("\n");
        writeContentInFile('./outputFiles/lowerCase.txt', lowerCaseData);
        writeFileNames('./filenames.txt', './outputFiles/lowerCase.txt\n');
        resolve(lowerCaseData);
    });
}


// 4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
function sorttheContentInFile(file) {
    return new Promise(function (resolve, reject) {
        const newFile = file.split("\n").sort().join("\n");
        writeContentInFile('./outputFiles/newFile.txt', newFile);
        writeFileNames('./filenames.txt', './outputFiles/newFile.txt');
        resolve(newFile);
    })
}


// 5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
function deletAllFilesInFilenames() {
    return new Promise(function (resolve, reject) {
        fs.readFile("./filenames.txt", "utf-8", (err, data) => {
            let fileName = data.split("\n");

            fileName.forEach((element) => {
                fs.unlink(element, (err) => {
                    if (err) {
                        throw err;
                    }

                    console.log("File is deleted.");
                });
            }
            );
        }
        );
    })
}



// Store File names into some other file
function writeFileNames(fileName, file) {
    fs.appendFileSync(fileName, file, function (err) {
        if (err) {
            console.log(err);
        } else {
            console.log('Successfully');
        }
    })
}
// Writing some content into some other file will
function writeContentInFile(filePath, content) {
    fs.writeFile(filePath, content, err => {
        if (err) console.log(err);
        else {
            return;
        }
    })
}

module.exports = {
    readGivenFile, convertToUpperCase, lowerCaseAndSplitInSentences,
    deletAllFilesInFilenames, sorttheContentInFile,
}