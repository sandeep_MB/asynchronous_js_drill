const fs = require('fs')

// delete a file
function deletedirectory(directoryName) {
    fs.unlink(directoryName, (err) => {
        if (err) {
            console.log(new Error('Could not delete directory ' + directoryName));
        }
        console.log("File is deleted.");
    });
}

//This Function will crerate files
function createFileAndDeleteSimultaneously(directoryName) {
    return new Promise(function (resolve, reject) {
        fs.writeFile(directoryName, 'Strings', err => {
            if (err) {
                reject('Error writing file', err);
            } else {
                console.log('Successfully wrote file')
                resolve(directoryName);
            }
        });
    })
}

//Create multiple files
function toCreateMultipleFiles(n){
    for(let index=0;index<n;index++){
        const path='../outputFiles/'+Math.random().toString(36).substr(2,5)+'.json';
        createFileAndDeleteSimultaneously(path)
        .then(directory => deletedirectory(directory))
        .catch((err) => console.log(new Error(err)));
    }
}
module.exports = { toCreateMultipleFiles };